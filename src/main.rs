use rand::seq::SliceRandom;
use std::env;

fn choose_char(before: char) -> char {
    let options = match before {
        'a' => vec!['b', 'c', 'd', 's', 'x'],
        'b' => vec!['a', 'e', 'i', 'o', 'u', 'l'],
        'c' => vec!['e', 'i', 'o', 'u', 'h'],
        'd' => vec!['a', 'e', 'i', 'o', 'u', 'y', 'r'],
        'e' => vec!['i', 'o', 'u', 'n', 'm', 'p', 't'],
        'f' => vec!['a', 'e', 'i', 'o', 'u'],
        'g' => vec!['a', 'e', 'i', 'o', 'u', 'r'],
        'h' => vec!['a', 'e', 'i', 'o', 'u'],
        'i' => vec!['a', 'e', 'o', 'u', 'n', 'd', 'g', 'm'],
        'j' => vec!['a', 'e', 'i', 'o', 'u'],
        'k' => vec!['a', 'e', 'i', 'o', 'u'],
        'l' => vec!['a', 'e', 'i', 'o', 'u', 'b'],
        'm' => vec!['a', 'e', 'i', 'o', 'u'],
        'n' => vec!['a', 'e', 'i', 'o', 'u'],
        'o' => vec!['i', 'u', 'p', 'n', 'g', 'x', 'l', 'q', 'v', 'z'],
        'p' => vec!['a', 'e', 'i', 'o', 'u'],
        'q' => vec!['a', 'e', 'i', 'o', 'u'],
        'r' => vec!['a', 'e', 'i', 'o', 'u'],
        's' => vec!['a', 'e', 'i', 'o', 'u'],
        't' => vec!['a', 'e', 'i', 'o', 'u', 'r'],
        'u' => vec!['a', 'e', 'i', 'o', 'v', 'f', 'l', 's', 'x', 'j'],
        'v' => vec!['a', 'e', 'i', 'o', 'u'],
        'w' => vec!['a', 'e', 'i', 'o', 'u'],
        'x' => vec!['a', 'e', 'i', 'o', 'u'],
        'y' => vec!['a', 'e', 'i', 'o', 'u'],
        'z' => vec!['a', 'e', 'i', 'o', 'u'],
        '#' => vec!['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        _ => vec!['E']
    };
    *options.choose(&mut rand::thread_rng()).unwrap()
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let len: u128;
    if args.len() > 1 {
        len = args[1].parse().unwrap_or(8);
    } else {
        len = 8;
    }
    
    let mut before_char = choose_char('#');
    let mut built = String::new();
    for _element in 0..len {
        let current_char = choose_char(before_char);
        built.push(current_char);
        before_char = current_char
    }
    println!("{}", built)
}
